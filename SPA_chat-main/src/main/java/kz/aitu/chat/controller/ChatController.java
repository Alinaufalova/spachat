package kz.aitu.chat.controller;

import kz.aitu.chat.repository.ChatRepository;
import kz.aitu.chat.service.ChatService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("api/v1/chats")
@AllArgsConstructor
public class ChatController {
    private ChatService chatService;
    private AuthService authService;

    @GetMapping("")
    public ResponseEntity<?> getAll(@RequestHeader("token") UUID token) {
        if (token == null) return ResponseEntity.badRequest().body("Empty authorization  token!");
        if (!authService.validateToken(token))
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Invalid authorization  token!");
        return ResponseEntity.ok(chatService.findAll());
    }


    @GetMapping("")
    public ResponseEntity<?> getAll(){
        return ResponseEntity.ok(chatService.findAll());
    }

    @GetMapping("/{chatid}/users")
    public ResponseEntity<?> getAllUsersByChatId(Long chatid){
        return ResponseEntity.ok(chatService.getUsersByChatId(chatid));
    }

    @DeleteMapping("/{chatid}")
    public ResponseEntity<?> deleteChat(@PathVariable Long id) {
        chatService.deleteById(id);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/{chatId}/users")
    public ResponseEntity<?> getAllUsersByChatId(@PathVariable Long chatId) {
        return ResponseEntity.ok(chatService.getAllUsersByChatId(chatId));
    }



}
