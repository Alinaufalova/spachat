package kz.aitu.chat.controller;

import kz.aitu.chat.repository.UsersRepository;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequestMapping("api/v1/users")
@AllArgsConstructor
public class UsersController {
    private UsersRepository usersService;
    private AuthService authService;

    @GetMapping("")
    public ResponseEntity<?> getAll(@RequestHeader("token") UUID token) {
        if (token == null) return ResponseEntity.badRequest().body("Empty auth token!");
        if (!authService.validateToken(token))
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Invalid auth token!");
        return ResponseEntity.ok(usersService.findAll());
    }


    @GetMapping("")
    public ResponseEntity<?> getAll(){
        return ResponseEntity.ok(usersRepository.findAll());
    }



}
