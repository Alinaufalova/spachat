package kz.aitu.chat.controller;

import kz.aitu.chat.model.Message;
import kz.aitu.chat.model.Participant;
import kz.aitu.chat.repository.ParticipantRepository;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/v1/participants")
@AllArgsConstructor
public class ParticipantController {
    private ParticipantRepository participantService;
    private AuthService authService;

    public ParticipantController() {
    }

    @GetMapping("")
    public ResponseEntity<?> getAll(@RequestHeader("token") UUID token) {
        if (token == null) return ResponseEntity.badRequest().body("Empty authorization token!");
        if (!authService.validateToken(token))
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Invalid authorization token!");
        return ResponseEntity.ok(participantService.findAll());
    }


    @GetMapping
    public ResponseEntity<?> findAllParticipant() {
        return ResponseEntity.ok(participantRepository.findAllParticipant());
    }

    @PostMapping
    public ResponseEntity<?> addParticipant(@RequestBody Participant participant) {
        return ResponseEntity.ok(participant.addParticipant(participant));
    }

    @DeleteMapping("{id}")
    public ResponseEntity<?> deleteParticipantById(@PathVariable Long id) {
        participantRepository.deleteParticipantById(id);
        return ResponseEntity.ok(id + " participant was deleted.");
    }

    @GetMapping("{id}")
    public ResponseEntity<?> findParticipantById(@PathVariable Long id) {
        participantRepository.findParticipantById(id);
        return ResponseEntity.ok(id + " participant was found.");


    }


