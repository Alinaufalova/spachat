package kz.aitu.chat.controller;

import kz.aitu.chat.model.Message;
import kz.aitu.chat.repository.MessageRepository;
import kz.aitu.chat.service.MessageService;
import lombok.AllArgsConstructor;
import org.apache.coyote.Response;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("api/v1/messages")
@AllArgsConstructor
public class MessageController {
    private MessageService messageService;
    private AuthService authService;

    @GetMapping("")
    public ResponseEntity<?> getAll(@RequestHeader("token") UUID token) {
        if (token == null) return ResponseEntity.badRequest().body("Empty authorization token!");
        if (!authService.validateToken(token))
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Invalid authorization token!");
        return ResponseEntity.ok(messageService.findAll());
    }


    @GetMapping
    public ResponseEntity<?> findAllMessages() {
        return ResponseEntity.ok(messageService.findAllMessages());
    }

    @PostMapping
    public ResponseEntity<?> addMessage(@RequestBody Message message) {
        return ResponseEntity.ok(messageService.addMessage(message));
    }

    @DeleteMapping("{id}")
    public ResponseEntity<?> deleteMessageById(@PathVariable Long id) {
        messageService.deleteMessageById(id);
        return ResponseEntity.ok(id + " message was deleted.");
    }

    @GetMapping("{id}")
    public ResponseEntity<?> findMessageById(@PathVariable Long id) {
        messageService.findMessageById(id);
        return ResponseEntity.ok(id + " message was found.");
    }

    @GetMapping("last10MessageByChatId/{id}")
    public ResponseEntity<?>  getLast10MessageByChatId(@PathVariable Long id) {
        messageService.getLast10MessageByChatId(id);
        return  ResponseEntity.ok("Returned last 10 messages");
    }

    @PostMapping("")
    public ResponseEntity<?> addMessage(@RequestBody Message message) throws Exception {
        try{
            messageService.addMessage(message);
        }
        catch (Exception e){
            return ResponseEntity.badRequest().body(e.getMessage());
        }
        return ResponseEntity.ok("Message added");

    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteMessage(@PathVariable Long id) {
        try {
            messageService.deleteById(id);
        } catch (Exception e) {
            return ResponseEntity.ok("Message deleted or not found " + id);
        }
        return ResponseEntity.ok("Deleted successfully:");

    }




}
