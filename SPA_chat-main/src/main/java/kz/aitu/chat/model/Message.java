package kz.aitu.chat.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;
import java.sql.Timestamp;


@Entity
@Table(name = "message")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Message {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Long user_id;
    private Long chat_id;
    private String text;
    private Long created_timestamp;
    private Long updated_timestamp;
    private boolean isDelivered;
    private boolean isRead;

    public void setCreated_timestamp(Long created_timestamp) {
        this.created_timestamp = created_timestamp;
    }

    public Long getId() {
        return id;
    }

    public Long getChatId() {
        return chat_id;
    }

    public Long getUserId() {
        return user_id;
    }

    public Long getCreated_timestamp() {
        return created_timestamp;
    }

    public Long getUpdated_timestamp() {
        return updated_timestamp;
    }


    public boolean isRead() {
        return isRead;
    }

    public boolean isDelivered() {
        return isDelivered;
    }



    @PrePersist
    protected void onCreate() {
        created_timestamp = updated_timestamp = new Date().getTime();
        isDelivered = false;
        isRead = false;
    }

    @PreUpdate
    protected void onUpdate() {
        updated_timestamp = new Date().getTime();
    }


    public void setText(Long text) {
        return text;
    }
}
