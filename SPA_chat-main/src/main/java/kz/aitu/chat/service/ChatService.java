package kz.aitu.chat.service;

import kz.aitu.chat.model.Chat;
import kz.aitu.chat.model.Participant;
import kz.aitu.chat.model.Users;
import kz.aitu.chat.repository.ChatRepository;
import kz.aitu.chat.repository.ParticipantRepository;
import kz.aitu.chat.repository.UsersRepository;
import lombok.AllArgsConstructor;
import org.apache.catalina.User;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@AllArgsConstructor

public class ChatService {
    private ChatRepository chatRepository;
    private ParticipantRepository participantRepository;
    private UsersRepository usersRepository;

    public List<Users> getUsersByChatId (Long chat_id){
        List<Participant> participants = participantRepository.findAllByChatId(chat_id);
        List<Users> users = new LinkedList<>();
        for(Participant participant : participants) {
            users.add(usersRepository.findById(participant.getUser_id()).get());
        }
        return users;
    }

    public List<Chat> findAll (){
        return chatRepository.findAll();
    }
}
