package kz.aitu.chat.service;


import kz.aitu.chat.repository.AuthRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor


public class AuthService {
    private UsersService usersService;
    private AuthRepository authRepository;

}
