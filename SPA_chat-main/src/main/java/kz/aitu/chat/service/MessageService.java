package kz.aitu.chat.service;


import kz.aitu.chat.model.Message;
import kz.aitu.chat.repository.MessageRepository;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Date;
import java.util.Optional;

@Service
@AllArgsConstructor
public class MessageService {
    private MessageRepository messageRepository;

    public List<Message> findAllMessages() {
        return messageRepository.findAll();
    }

    public Message addMessage(Message message) {
        changeDate(message);
        return messageRepository.save(message);
    }

    public Message update(Message updatedMessage) throws Exception {
        if (updatedMessage == null) throw new Exception("Message is null");
        if (updatedMessage.getId() == null) throw new Exception("Message id is null");
        if (updatedMessage.getText() == null || updatedMessage.getText().isBlank()) throw new Exception("Message text is blank or null");

        if (!participantService.existsByChatIdAndUserId(updatedMessage.getChatId(), updatedMessage.getUserId())) {
            throw new Exception("Do not have access");
        }

        Optional<Message> messageDb = messageRepository.findById(updatedMessage.getId());
        if (messageDb.isEmpty()) throw new Exception("Message was not found");
        updatedMessage.setText(updatedMessage.getText());
        Message message = messageDb.get();
        message.setText(updatedMessage.getText());
        return save(message);
    }


    public void deleteMessageById(Long id) {
        messageRepository.deleteById(id);
    }

    public Message findMessageById(Long id) {
        return messageRepository.findById(id).get();
    }

    public void changeDate(Message message) {
        Date date = new Date();
        message.setCreated_timestamp(date.getTime());
    }

    public List<Message> getLast10MessageByChatId(Long chatId) {
        List<Message> newMessages = new ArrayList<>();
        List<Message> messages = messageRepository.getAllTextByChatId(new PageRequest(0, 10, Sort.Direction.ASC, "chatId"));
        for (Message message : messages) {
            newMessages.add(messageRepository.findById(message.getId()).get());
        }
        return newMessages;

    }


}