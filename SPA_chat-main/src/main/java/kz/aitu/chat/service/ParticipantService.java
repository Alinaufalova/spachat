package kz.aitu.chat.service;


import kz.aitu.chat.model.Message;
import kz.aitu.chat.repository.MessageRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor


public class ParticipantService {
    private ParticipantService participantService;

    public List<Message> findAllMessages() {
        return participantService.findAll();
    }

    public Message addMessage(Message message) {
        changeDate(message);
        return participantService.save(message);
    }

    public void deleteMessageById(Long id) {
        participantService.deleteById(id);
    }

    public Message findMessageById(Long id) {
        return participantService.findById(id).get();
    }
}
