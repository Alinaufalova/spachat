package kz.aitu.chat.service;

import kz.aitu.chat.model.Chat;
import kz.aitu.chat.model.Participant;
import kz.aitu.chat.model.Users;
import kz.aitu.chat.repository.ChatRepository;
import kz.aitu.chat.repository.ParticipantRepository;
import kz.aitu.chat.repository.UsersRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class UsersService {

    private ChatRepository chatRepository;
    private ParticipantRepository participantRepository;
    private UsersRepository usersRepository;

    public List<Users> findAll() {
        return usersRepository.findAll();
    }

    public Optional<Users> findById(Long id) {
        return usersRepository.findById(id);
    }

    public Users save(Users user) {
        return usersRepository.save(user);
    }

    public void deleteById(Long id) {
        usersRepository.deleteById(id);
    }

    public List<Chat> getAllChatsByUserId(Long user_id) {
        List<Participant> participantList = participantRepository.findAllByUserId(user_id);
        List<Chat> chatList = new ArrayList<>();
        for (Participant participant : participantList) {
            Optional<Chat> chatOptional = chatRepository.findById(participant.getChatId());
            if (chatOptional.isPresent()) {
                chatList.add(chatOptional.get());
            }
        }
        return chatList;
    }

    public Long addUser(Users user) throws Exception {
        if (user == null) throw new Exception("User is null");
        if(user.getName() == null || user.getName().isBlank()) throw new Exception("Null");
        save(user);
        return user.getId();
    }

}
